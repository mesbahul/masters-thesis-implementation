#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

#include "../config/var.h"

//#define BUFLEN 1500
//#define BLCK_LEN 1470
#define DTPORT 4690 // port for upload data
#define SERVER_IP "10.0.0.1"
#define FILE_NAME "file_data.txt"

int sfd;

// packet structure
struct TDMAControlPacket {
    int type;
    char id[18];
    //int offset;
    int slot_duration;
    //int round_time;
};

struct UdpPacket {
    int pck_id;
    char id[18];
    char dtbuf[BLCK_LEN + 1];
    //char dtbuf[65000];
    long send_time_sec;
    long send_time_usec;
    int end_of_transmission;
};

/* this function used for error handling */
void diep(char *s) {
    perror(s);
    close(sfd);
    exit(1);
}

// this function creates a socket and return the file descriptor
int create_socket(int domain, int type, int protocol, int isBroadcast) {
    int sfd;
    int broadcast = 1;
    struct timeval tv;
    tv.tv_sec = 10;
    tv.tv_usec = 500000;
    if((sfd = socket(domain, type, protocol)) == -1)
        diep("socket");

    if(setsockopt(sfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
        diep("timeout");
    }
    
    if(isBroadcast) {
        if (setsockopt(sfd, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof broadcast) == -1)
            diep("broadcast");
    }
    
    return sfd;
}

struct sockaddr_in manage_sockaddr_in_recv(int port) {
    struct sockaddr_in saddr;
    bzero(&saddr, sizeof(saddr)); //initialize
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(port);
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);
    
    return saddr;
}

int main() {
    int sfd;
    socklen_t len;
    char line[BUFLEN];
    struct UdpPacket *pck;
    struct timeval start, end, start1;
    
    int pack_count = 0;
    /*
    int pack_id[BUFLEN];
    int client_id[BUFLEN];
    long send_ts_sec[BUFLEN];
    long send_ts_usec[BUFLEN];
    long recv_ts_sec[BUFLEN];
    long recv_ts_usec[BUFLEN];
    */
    
    long double curr_rx_ts, prev_rx_ts;

    long execution_start, execution_end;
    long proc_time;
    int num_duplicate_packets;

    //int BLCK_LEN;

    //if(argc != 2) {
        //  printf("Usage: %s BlockLength\n", argv[0]);
        //  return -1;
        //}

        //BLCK_LEN = atoi(argv[1]);
        //printf("%d", BLCK_LEN);
    
    FILE *file; // file for recording packet information
    
    /* 
     * socketaddr_in is a structure containing an Internet socket address.
     * It contains: an address family, a port number, an IP address
     * server will listen in 'saddr' socket and client socket is 'caddr'
     */
    struct sockaddr_in saddr, caddr;
    
    /* 
     * Create a socket.
     * AF_INET says that it will be an Internet socket.
     * SOCK_DGRAM says that it will use datagram delivery instead of virtual circuits.
     * IPPROTO_UDP says that it will use the UDP protocol
     */
    sfd = create_socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, 0);
    //if((sfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    //  diep("socket");
    
    saddr = manage_sockaddr_in_recv(DTPORT);
    // Initialize saddr strucure, filling with binary zeros
    //bzero(&saddr, sizeof(saddr));
    
    //saddr.sin_family = AF_INET;
    //saddr.sin_port = htons(PORT); // htons() ensures that the byte order is correct (Host TO Network order/Short integer)
    
    //if (inet_aton(SERVER_IP, &saddr.sin_addr) == 0) {
    //  fprintf(stderr, "inet_aton() failed.\n");
    //  exit(1);
    //}
    
    // the socket sfd should be bound to the address in saddr.
    if(bind(sfd, (struct sockaddr *)&saddr, sizeof(saddr)) == -1)
        diep("bind");
    
    //file = fopen("log_data.txt", "w");
    //fprintf(file, "Client\tPCKNO\tSend_TS\t\t\t\tRECV_TS\n");
    //fclose(file);

    len = sizeof(caddr);
    curr_rx_ts = prev_rx_ts = 0;
    
    pck  = malloc(sizeof(struct UdpPacket));

    printf("Server is waiting for data... ...\n");

    printf("ID\tTotal (DUP)\tDuration\tData Transferred\tBandwidth\n");
    
    char recvbuf[BLCK_LEN + 1]; 
    
    int success = 0;
    int pck_recv_count = 0;
    int pck_recv_total_count = 0;
    num_duplicate_packets = 0;
    int pck_recv_actual_count = 0;
    //while(pack_count < BUFLEN){
    while(success == 0){
        /* Recieve file from client */
        //char* fr_name = FILE_NAME;
        
//      FILE *fr = fopen(FILE_NAME, "a");
        
//      if(fr == NULL) diep("FILE NOT OPENED:");
//      else {
            bzero(recvbuf, BLCK_LEN + 1);
            int fr_block_sz, real_packet_size, i = 0;
            int new_id, old_id = -1;
            int iteration = 0;
            
            gettimeofday(&start, NULL);
            gettimeofday(&start1, NULL);

            int dup_pack, out_order_pack = 0;
	    proc_time = 0;

            while(1) {
                //proc_time = 0;
                
                if((fr_block_sz = recvfrom(sfd, line, sizeof(struct UdpPacket), 0, (struct sockaddr *)&caddr, &len)) == -1) {
                    execution_start = start.tv_usec / 1000000 + start.tv_sec;
                    execution_end = end.tv_usec / 1000000 + end.tv_sec;
                    long execution_total = execution_end - execution_start;
                    //double actual_count = pck_recv_actual_count;
                    //double percentage_dup = dup_pack / actual_count;
                    //printf("%f\n", percentage_dup);
                    //percentage_dup = percentage_dup * 100;
                    printf("Total Received %d | Actual %d | Duplicate %d | Out of order %d | Execution Time %ld sec\n", pack_count, pck_recv_actual_count, dup_pack, out_order_pack, execution_total);
                    diep("recvfrom()");
                }

                pck = (struct UdpPacket *)line;
                //printf("recieved size %d (%d)\n", sizeof(*pck), fr_block_sz);

                if(proc_time != 0){
			//gettimeofday(&start1, NULL);
			start1.tv_sec = pck->send_time_sec;
			start1.tv_usec = pck->send_time_usec;
			proc_time = 0;
		}

		// getting the receiveing timestamp
                gettimeofday(&end, NULL);

//              strcpy(recvbuf, pck->dtbuf);
                real_packet_size = fr_block_sz;
                fr_block_sz = sizeof(pck->dtbuf);
                //printf("recieved size %d (%d)\n", sizeof(*pck), fr_block_sz);
                
                if(fr_block_sz == 0) break;

                new_id = pck->pck_id;
                if(new_id != old_id && pck->end_of_transmission == 0) {
                    if(new_id < old_id){
                        //printf("Out %d, ", new_id);
                        out_order_pack++;
                    }
                    //else printf("New %d, ", new_id);
                    
                    pck_recv_count++;
                    old_id = new_id;
                }
                else if(new_id == old_id && pck->end_of_transmission == 0) {
                    dup_pack++;
                    //printf("DUP | %s | %d | %ld,%ld | %ld,%ld\n", pck->id, pck->pck_id, pck->send_time_sec, pck->send_time_usec, end.tv_sec, end.tv_usec);
                    //;
                }               
//              int write_sz = fwrite(recvbuf, sizeof(char), fr_block_sz, fr);
                    
//              if(write_sz < fr_block_sz) printf("File write failed on server.\n");
                else if(pck->end_of_transmission == 9) {
		    proc_time = ((pck->send_time_sec * 1000000 + pck->send_time_usec) - (start1.tv_sec * 1000000 + start1.tv_usec));
                    //proc_time = ((end.tv_sec * 1000000 + end.tv_usec) - (start1.tv_sec * 1000000 + start1.tv_usec));

                    //gettimeofday(&start1, NULL);
                    
                    //proc_time = ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec));
//                  printf("%s | %d | %d | %ld,%ld | %ld,%ld\n", pck->id, pack_count, pck_recv_count, pck->send_time_sec, pck->send_time_usec, end.tv_sec, end.tv_usec);
                    
                    double total_data = pck_recv_total_count * real_packet_size; //(BLCK_LEN + 1);
                    
                    double data_transferred = total_data/(1024 * 1024);
                    double bandwidth = (data_transferred * 8 * 1000000) / proc_time;
                    
                    printf("%s | %d | %d | %d | %d | %ld | %f | %f\n", pck->id, pck_recv_count, pck_recv_total_count - pck_recv_count, out_order_pack, dup_pack, proc_time, data_transferred, bandwidth);
                    //printf("%s | %d | %d\n", pck->id, pck_recv_count, pck_recv_total_count - pck_recv_count);
                    //printf("%d, ", pck_recv_total_count - pck_recv_count);
                    
                    num_duplicate_packets = num_duplicate_packets + (pck_recv_total_count - pck_recv_count);
                    pck_recv_total_count = 0;
                    pck_recv_actual_count += pck_recv_count; // total actual packets
                    pck_recv_count = 0; // actual packets in one time slot
                    //out_order_pack = 0;
		    //dup_pack = 0;
                    old_id = new_id = -1;
                }

                i++;
                pack_count++; // total packets recved by receiver
                pck_recv_total_count++; // packet recv from one time slot
            }
            if(fr_block_sz < 0) {
                        if (errno == EAGAIN) printf("recv() timed out.\n");
                        else diep("recv failed:");
            }
            
            printf("OK recieved from client!\n");
//          fclose(fr);
//          }
            success = 1;
                
    }   

    close(sfd);
    return 0;
}
