#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>

#include "../config/var.h" // constant variables

#define DTPORT 4690 // port for upload data

int sfd;

// Structure of the UDP Packet
struct UdpPacket {
    int pck_id;
    int id;
    char dtbuf[BLCK_LEN];
    long send_time_sec;
    long send_time_usec;
};

/* this function used for error handling */
void diep(char *s) {
    perror(s);
    close(sfd);
    exit(1);
}

// this function creates a socket and return the file descriptor
int create_socket(int domain, int type, int protocol, int isBroadcast) {
    int broadcast = 1;
    struct timeval tv;
    tv.tv_sec = 10;
    tv.tv_usec = 500000;
    if((sfd = socket(domain, type, protocol)) == -1)
        diep("socket");

    if(setsockopt(sfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
        diep("timeout");
    }
    
    if(isBroadcast) {
        if (setsockopt(sfd, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof broadcast) == -1)
            diep("broadcast");
    }
    
    return sfd;
}

struct sockaddr_in manage_sockaddr_in_recv(int port) {
    struct sockaddr_in saddr;
    bzero(&saddr, sizeof(saddr)); //initialize
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(port);
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);
    
    return saddr;
}

int main(int argc, char const *argv[])
{
    /* code */
    int len;
    struct sockaddr_in saddr, caddr;
    struct timeval start, end;
    char line[BUFLEN];
    int numbytes;
    long proc_time, transfer_time;

    struct UdpPacket *pck;
    pck  = malloc(sizeof(struct UdpPacket));

    sfd = create_socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, 0);
    saddr = manage_sockaddr_in_recv(DTPORT);

    // the socket sfd should be bound to the address in saddr.
    if(bind(sfd, (struct sockaddr *)&saddr, sizeof(saddr)) == -1)
        diep("bind");

    len = sizeof(caddr);

    printf("Server is waiting for data... ...\n");

    int success = 0;
    
    while(1){
        gettimeofday(&start, NULL);    

        /*
         * Receive a packet from sfd, that the data should be put into line
         * line can store at most BUFLEN characters
         * The zero parameter says that no special flags should be used
         * Data about the sender should be stored in saddr, which has room for len byte
         */
        if ((numbytes = recvfrom(sfd, line, sizeof(struct UdpPacket), 0, (struct sockaddr *)&caddr, &len)) == -1)
            diep("recvfrom()");

        gettimeofday(&end, NULL); // receiving timestamp

        /* calculating packet receive process time */
        //proc_time = ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec)) - SLEEP_TIME;

	/* calculating packet transfer time  */
        transfer_time = ((end.tv_sec * 1000000 + end.tv_usec) - (pck->send_time_sec * 1000000 + pck->send_time_usec));

        pck = (struct UdpPacket *)line;

        //printf("%d | %d | %ld | %ld\n", pck->pck_id, BLCK_LEN, transfer_time, proc_time);
	printf("%ld, ", transfer_time);
    }

    close(sfd);

    return 0;
}
