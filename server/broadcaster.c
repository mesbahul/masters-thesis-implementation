#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>

#include "../config/var.h"

#define BPORT 9930 // use for broadcasting
#define JPORT 9090 // server is listening the join request

#define MAXCLIENT 10
#define TIME_FRAME 90000 //in micro seconds = variable
#define CONTENTION 10000 // in microseconds = 10 ms
#define GUARD_TIME 2000  // in microseconds = 2 ms

int sfd, quit_count;

char client_mac_arr[MAXCLIENT][18];

struct TDMAControlPacket
{
    int type;
    char id[18];
    int slot_duration;
};

struct UdpPacket
{
    int pck_id;
    char id[18];
    char dtbuf[BLCK_LEN + 1];
    long send_time_sec;
    long send_time_usec;
    int end_of_transmission;
};

// place some possible error checking here !!!
struct TDMAControlPacket set_tdma_packet(int pckType, char *id, int duration)
{
    struct TDMAControlPacket pck;
    bzero(&pck, sizeof(pck));
    pck.type = pckType;
    strcpy(pck.id, id);
    pck.slot_duration = duration;
    
    return pck;
}


/* this function used for error handling */
void diep(char *s)
{
    perror(s);
    close(sfd);
    exit(1);
}

// this function creates a socket and return the file descriptor
int create_socket(int domain, int type, int protocol, int isBroadcast)
{
    int sfd;
    int broadcast = 1;
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 100000;
    if((sfd = socket(domain, type, protocol)) == -1)
        diep("socket");
    
    if(!isBroadcast)
    {
        if (setsockopt(sfd, SOL_SOCKET, SO_RCVTIMEO, (struct timeval *)&tv, sizeof (struct timeval)) == -1)
            diep("timeout");
    }
    else
    {
        if(setsockopt(sfd, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof broadcast) == -1)
            diep("broadcast");
    }
    
    return sfd;
}

struct sockaddr_in manage_sockaddr_in_send(char *ip_address, int port) {
    struct sockaddr_in saddr;
    bzero(&saddr, sizeof(saddr)); //initialize
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(port); // htons() ensures that the byte order is correct (Host TO Network order/Short integer)
    if(inet_aton(ip_address, &saddr.sin_addr.s_addr) == 0)
        diep("inet_aton");
        
    return saddr;
}

struct sockaddr_in manage_sockaddr_in_recv(int port) {
    struct sockaddr_in saddr;
    bzero(&saddr, sizeof(saddr)); //initialize
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(port);
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);
    
    return saddr;
}

int check_client_id(char *id)
{
    int found = 0;
    int counter;
    for(counter = 0; counter < MAXCLIENT; counter++)
    {
        if(strcmp(id, client_mac_arr[counter]) == 0)
        {
            found = 1;
            return found;
        }
    }
    return found;
}

// command line parameter should be broadcast address
int main(int argc, char **argv)
{
    int count, client_count;
    int numbytes;
    char line[BUFLEN];
    socklen_t len;
    long proc_time, timeout;
    struct timeval start, end, val1, timeo1, timeo2;
    struct TDMAControlPacket pck;
    struct UdpPacket *udpPack;
    struct TDMAControlPacket *jpck;
    
    for(count = 0; count < MAXCLIENT; count++)
          memset(&client_mac_arr[count],'\0',18);
    
    /* 
     * socketaddr_in is a structure containing an Internet socket address.
     * It contains: an address family, a port number, an IP address
     */
    struct sockaddr_in caddr, saddr;
    
    if(argc != 2) {
        printf("Usage: %s HostAddress\n", argv[0]);
        return -1;
    }

    /* 
     * Create a socket.
     * AF_INET says that it will be an Internet socket.
     * SOCK_DGRAM says that it will use datagram delivery instead of virtual circuits.
     * IPPROTO_UDP says that it will use the UDP protocol
     */
    sfd = create_socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, 1);
    
    caddr = manage_sockaddr_in_send(argv[1], BPORT);
    
    // broadcasting becaon packet
    client_count = 0;
    quit_count = 0;
        
    while(1)
    {
        /* 
         * broadcasting contention packet 
         */
        pck = set_tdma_packet(0, "NO", (CONTENTION - GUARD_TIME));      
        len = sizeof(caddr);
        
        gettimeofday(&start, NULL);
        
        if ((numbytes = sendto(sfd, (const char *)&pck, sizeof(pck), 0, (struct sockaddr *)&caddr, len)) == -1)
            diep("sendto() Contention");
        
        
        //printf("Contention Send at %ld,%ld sec\n", start.tv_sec, start.tv_usec);
        printf("\nContention Send | %ld,%ld | %d bytes packet\n", start.tv_sec, start.tv_usec, sizeof(pck));
        /* 
         * listening Join request from client
         */
        close(sfd);
        sfd = create_socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, 0);
        caddr = manage_sockaddr_in_recv(JPORT);
        
        // the socket sfd should be bound to the address in caddr.
        if(bind(sfd, (struct sockaddr *)&caddr, sizeof(caddr)) == -1)
            diep("bind");
        
        proc_time = 0;
        //bzero(&udpPack, sizeof(udpPack)); // should be removed if OK without this line
        bzero(&jpck, sizeof(jpck));
        len = sizeof(caddr);

        while(1)
        {
            gettimeofday(&end, NULL);
            proc_time = ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec));
            
            //printf("%ld\n", proc_time);
            
            if(proc_time >= CONTENTION)
                break;
            if(client_count < 2){
                numbytes = recvfrom(sfd, line, sizeof(struct TDMAControlPacket), 0, (struct sockaddr *)&saddr, &len);
            
                //udpPack = (struct UdpPacket *)line;
                jpck = (struct TDMAControlPacket *)line;
                line[numbytes] = '\0';
            
                printf("Join Request | %s | %ld,%ld | packet size %d (%d)\n", jpck->id, end.tv_sec, end.tv_usec, sizeof(*jpck), numbytes);
            
                if(!check_client_id(jpck->id))
                {
                    strcpy(client_mac_arr[client_count], jpck->id);
                    //printf("New Client: %d - %s Added!\n", client_count, client_mac_arr[client_count]);
                    client_count++;
                }
                else
                    printf("Client already exist: %s\n", client_mac_arr[client_count - 1]);
            }
        }
        
        //printf("Contention Slot | %ld\n\n", proc_time);
                
        // calculating time slot duration based on clients
        int time_slot_duration;     
        time_slot_duration = (TIME_FRAME - CONTENTION) / client_count;  
        
        //printf("time slot per client is %d\n", time_slot_duration);
        
        /* 
         * broadcasting control packet
         */
        count = 0;
        while(client_mac_arr[count] != '\0' && count < client_count)
        {
            close(sfd);
            sfd = create_socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, 1);
            caddr = manage_sockaddr_in_send(argv[1], BPORT);
            len = sizeof(caddr);
            
            pck = set_tdma_packet(1, client_mac_arr[count], (time_slot_duration - GUARD_TIME));
            
            if ((numbytes = sendto(sfd, (const char *)&pck, sizeof(pck), 0, (struct sockaddr *)&caddr, len)) == -1)
                diep("sendto() Control");
            
            gettimeofday(&val1, NULL);
            //printf("Sending Control to Client %s, time: %ld,%ld\n", pck.id, val1.tv_sec, val1.tv_usec);
            printf("Control Send | %s | %ld,%ld | packet size %d\n", pck.id, val1.tv_sec, val1.tv_usec, numbytes);
            
            close(sfd);
            
            // wait for end_of_transmission notification from client            
            sfd = create_socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, 0);
            caddr = manage_sockaddr_in_recv(JPORT);
            
            // the socket sfd should be bound to the address in caddr.
            if(bind(sfd, (struct sockaddr *)&caddr, sizeof(caddr)) == -1)
                diep("bind");
            
            proc_time = 0;
            bzero(&udpPack, sizeof(udpPack)); // should be removed if OK without this line
            bzero(&jpck, sizeof(jpck));
            len = sizeof(caddr);
            
            // receiving notification from clients            
            gettimeofday(&timeo1, NULL);
            while(1)
            {
                numbytes = recvfrom(sfd, line, sizeof(struct UdpPacket), 0, (struct sockaddr *)&saddr, &len);
                udpPack = (struct UdpPacket *)line;
                //numbytes = recvfrom(sfd, line, sizeof(struct TDMAControlPacket), 0, (struct sockaddr *)&saddr, &len);
                //jpck = (struct TDMAControlPacket *)line;
                line[numbytes] = '\0';
            
                if(strcmp(udpPack->id, client_mac_arr[count]) == 0 && udpPack->end_of_transmission == 1) {
                    printf("GET NOTIFICATION | %s | %d | %ld,%ld\n", udpPack->id, udpPack->end_of_transmission, udpPack->send_time_sec, udpPack->send_time_usec);
                    break;
                }

                gettimeofday(&timeo2, NULL);
                timeout = ((timeo2.tv_sec * 1000000 + timeo2.tv_usec) - (timeo1.tv_sec * 1000000 + timeo1.tv_usec));
                if(timeout > time_slot_duration + CONTENTION) { 
                    quit_count++;
                    break;
                }
            }            
            count++;
        }
    }

    close(sfd);

    return 0;
}
