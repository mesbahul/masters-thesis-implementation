#!/bin/bash

clear

#gcc -o myclient myclient.c

current=$(date +"%H%M%S%6N")
slp=$(( $1 - current ))
slp1=$(( slp / 1000000 ))
echo $current
echo $slp

n=`echo "scale=6; $slp / 1000000 " | bc`

echo $n

sleep `echo $n"s"`

#./newlistener_no_tdma
./notdma_sender

#echo "Done!"
