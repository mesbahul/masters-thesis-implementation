/*
 * packet_sender.c
 * author: mesbahul islam
 * date: 24th January 2016
 * 
 * This program is for calculating how much time need for a UDP packet to transfer between two nodes
 *
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
//#include <sys/time.h>
#include <linux/if.h>
#include "../config/var.h" // constant variables

#define DTPORT 4690 // port for upload data
#define SERVER_IP "10.0.0.23"

// Structure of the UDP Packet
struct UdpPacket {
    int pck_id;
    int id;
    char dtbuf[BLCK_LEN];
    long send_time_sec;
    long send_time_usec;
};


struct UdpPacket set_tdma_packet(int pck_no, char *databuffer, int id, struct timeval ts) {
    struct UdpPacket pck;
    bzero(&pck, sizeof(pck));
    
    pck.id = id;
    pck.pck_id = pck_no;
    strcpy(pck.dtbuf, databuffer);
    pck.send_time_sec = ts.tv_sec;
    pck.send_time_usec = ts.tv_usec;
    
    return pck;
}


/* this function used for error handling */
void diep(char *s) {
    perror(s);
    exit(1);
}

// this function creates a socket and return the file descriptor
int create_socket(int domain, int type, int protocol, int isBroadcast) {
    int sfd;
    int broadcast = 1;
    
    if((sfd = socket(domain, type, protocol)) == -1)
        diep("socket");

    if(isBroadcast) {
       if (setsockopt(sfd, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof broadcast) == -1)
            diep("broadcast");
    }

    return sfd;
}

struct sockaddr_in manage_sockaddr_in_send(char *ip_address, int port) {
    struct sockaddr_in saddr;
    bzero(&saddr, sizeof(saddr)); //initialize
    
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(port); // htons() ensures that the byte order is correct (Host TO Network order/Short integer)
    
    if(inet_aton(ip_address, &saddr.sin_addr.s_addr) == 0)
        diep("inet_aton");

    return saddr;
}

struct sockaddr_in manage_sockaddr_in_recv(int port) {
    struct sockaddr_in saddr;
    bzero(&saddr, sizeof(saddr)); //initialize
    
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(port);
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);

    return saddr;
}

// get the MAC address
static char *interface_mactoa() {
    static char buff[256];

    struct ifreq s;
    int fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);

    strcpy(s.ifr_name, "wlan0");
    if (0 == ioctl(fd, SIOCGIFHWADDR, &s)) {
        unsigned char *ptr = (unsigned char*) s.ifr_addr.sa_data;

        sprintf(buff, "%02X:%02X:%02X:%02X:%02X:%02X",
            (ptr[0] & 0xff), (ptr[1] & 0xff), (ptr[2] & 0xff),
            (ptr[3] & 0xff), (ptr[4] & 0xff), (ptr[5] & 0xff));
    }

    return (buff);
}


int main(int argc, char const *argv[])
{
    int sfd; /* socket */
    socklen_t len;
    long proc_time; /* total process time */
    struct timeval send_t, start, end, start_total, end_total; /* for recording timestamp */

    /*
     * socketaddr_in is a structure containing an Internet socket address.
     * It contains: an address family, a port number, an IP address
     * client will listen in 'caddr' socket and server socket is 'saddr'
     */
    struct sockaddr_in saddr, caddr;

    struct UdpPacket data; /* UDP packet to be sent in busy loop */

    int pck_no = 0;

    char mac_addr[18]; /* id of the client */
    memset(&mac_addr, '\0', 18);
    
    // Initializing and filling the data to be sent
    char sdbuf[BLCK_LEN];
    bzero(sdbuf, BLCK_LEN);
    
    int newChar;    
    for(newChar = 0; newChar < BLCK_LEN - 1; newChar++) {
        sdbuf[newChar] = 'a';
    }

    sdbuf[BLCK_LEN - 1] = '\0';

    /*
     * Create a socket.
     * AF_INET says that it will be an Internet socket.
     * SOCK_DGRAM says that it will use datagram delivery instead of virtual circuits.
     * IPPROTO_UDP says that it will use the UDP protocol
     */
    sfd = create_socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, 0);

    caddr = manage_sockaddr_in_send(SERVER_IP, DTPORT);
    len = sizeof(caddr);

    /* get start timestamp */
    gettimeofday(&send_t, NULL);

    /* initializing the UDP packet */
    //strcpy(mac_addr, interface_mactoa());
    data = set_tdma_packet(pck_no, sdbuf, 1, start);

    /* now send data to the server in busy loop */
    gettimeofday(&start_total, NULL);
    while(pck_no < 25000){
        gettimeofday(&send_t, NULL); /* getting the send timestamp */

        pck_no++;
        data.pck_id = pck_no;                            
        data.send_time_sec = send_t.tv_sec;
        data.send_time_usec = send_t.tv_usec;

        gettimeofday(&start, NULL); /* packet send start */

        if(sendto(sfd, (const char *)&data, sizeof(data), 0, (struct sockaddr *)&caddr, len) == -1)
            diep("sendto UDP packet:");

        gettimeofday(&end, NULL); /* packet send end */

        /* calculating packet process time */
        proc_time = ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec));
        /* printing necessary information - client id, packet id, sending timestamp, packet processing time */
        printf("INFO | %d | %d | %ld,%ld | %ld | packet size %d\n", data.id, data.pck_id, send_t.tv_sec, send_t.tv_usec, proc_time, sizeof(data));

	usleep(SLEEP_TIME); // 100 ms sleep time
    }
    gettimeofday(&end_total, NULL);

    //printf("%d (%d)\n", sizeof(data), pck_no);
    double total_data = sizeof(data) * pck_no * 8;
    long total_time = ((end_total.tv_sec * 1000000 + end_total.tv_usec) - (start_total.tv_sec * 1000000 + start_total.tv_usec));
    total_time = total_time - (pck_no * SLEEP_TIME);
    double bandwidth = total_data  / total_time;
    bandwidth = bandwidth * 1000000;
    bandwidth = bandwidth / (1024 * 1024);

    printf("total data: %f, Total time: %ld, Bandwidth: %f\n", total_data, total_time, bandwidth);

    close(sfd);

    return 0;
}
