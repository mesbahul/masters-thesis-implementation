#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <linux/if.h>

#include "../config/var.h" // holds all necessary constant variables

#define TOTAL_DATA_SIZE 100

#define BPORT 9930 // use for broadcasting
#define JPORT 9090 // server is listening the join request
#define DTPORT 4690 // port for upload data
#define SERVER_IP "10.0.0.1"
#define FILE_NAME "experiment_data.txt"

#define GUARD 0

//int sfd;

struct TDMAControlPacket
{
    int type;
    char id[18];
    int slot_duration;
};

struct UdpPacket
{
    int pck_id;
    char id[18];
    char dtbuf[BLCK_LEN + 1];
    long send_time_sec;
    long send_time_usec;
    int end_of_transmission;
};

// place some possible error checking here !!!
struct UdpPacket set_tdma_packet(int pck_no, char *databuffer, char *id, int end_tx, struct timeval ts)
{
    struct UdpPacket pck;
    bzero(&pck, sizeof(pck));

    pck.pck_id = pck_no;
    strcpy(pck.id, id);
    strcpy(pck.dtbuf, databuffer);
    pck.send_time_sec = ts.tv_sec;
    pck.send_time_usec = ts.tv_usec;
    pck.end_of_transmission = end_tx;

    return pck;
}


struct TDMAControlPacket set_control_packet(int pckType, char *id, int duration)
{
    struct TDMAControlPacket pck;
    bzero(&pck, sizeof(pck));
    pck.type = pckType;
    strcpy(pck.id, id);
    pck.slot_duration = duration;

    return pck;
}


/* this function used for error handling */
void diep(char *s)
{
    perror(s);
    //close(sfd);
    exit(1);
}

// this function creates a socket and return the file descriptor
int create_socket(int domain, int type, int protocol, int isBroadcast)
{
    int sfd;
    int broadcast = 1;
    if((sfd = socket(domain, type, protocol)) == -1)
        diep("socket");

    if(isBroadcast)
    {
        if (setsockopt(sfd, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof broadcast) == -1)
            diep("broadcast");
    }

    return sfd;
}

struct sockaddr_in manage_sockaddr_in_send(char *ip_address, int port)
{
    struct sockaddr_in saddr;
    bzero(&saddr, sizeof(saddr)); //initialize
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(port); // htons() ensures that the byte order is correct (Host TO Network order/Short integer)
    if(inet_aton(ip_address, &saddr.sin_addr.s_addr) == 0)
        diep("inet_aton");

    return saddr;
}

struct sockaddr_in manage_sockaddr_in_recv(int port)
{
    struct sockaddr_in saddr;
    bzero(&saddr, sizeof(saddr)); //initialize
    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(port);
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);

    return saddr;
}

// get the MAC address
static char *interface_mactoa()
{
    static char buff[256];

    struct ifreq s;
    int fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);

    strcpy(s.ifr_name, "wlan0");
    if (0 == ioctl(fd, SIOCGIFHWADDR, &s))
    {
        unsigned char *ptr = (unsigned char*) s.ifr_addr.sa_data;

        sprintf(buff, "%02X:%02X:%02X:%02X:%02X:%02X",
          (ptr[0] & 0xff), (ptr[1] & 0xff), (ptr[2] & 0xff),
          (ptr[3] & 0xff), (ptr[4] & 0xff), (ptr[5] & 0xff));
    }

    return (buff);
}

int main()
{
    int sfd; 
    socklen_t len;
    char line[BUFLEN];
    struct TDMAControlPacket *pck;

    int pack_count;
    int numbytes;
    int slot_time;
    int join = 0;
    int quit = 0;
    char mac_addr[18];
    memset(&mac_addr, '\0', 18);

    long proc_time, prev_proc_time;
    struct timeval start, end, t1, t2;

    /*
     * socketaddr_in is a structure containing an Internet socket address.
     * It contains: an address family, a port number, an IP address
     * client will listen in 'caddr' socket and server socket is 'saddr'
     */
    struct sockaddr_in saddr, caddr;

    /*
     * Create a socket.
     * AF_INET says that it will be an Internet socket.
     * SOCK_DGRAM says that it will use datagram delivery instead of virtual circuits.
     * IPPROTO_UDP says that it will use the UDP protocol
     */
    sfd = create_socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, 0);

    caddr = manage_sockaddr_in_recv(BPORT);

    // the socket sfd should be bound to the address in caddr.
    if(bind(sfd, (struct sockaddr *)&caddr, sizeof(caddr)) == -1)
            diep("bind");

    len = sizeof(caddr);

    pck  = malloc(sizeof(struct TDMAControlPacket));

    printf("listener: waiting to recvfrom...\n\n");

    int pck_no = 1; // refers to the continuous packet number
    int count = 0;

    // Creating data of size BLCK_LEN
    printf("%d\n", BLCK_LEN);
    char sdbuf[BLCK_LEN];
    bzero(sdbuf, BLCK_LEN + 1);
    int newChar;    
    for(newChar = 0; newChar < BLCK_LEN; newChar++)
    {
        sdbuf[newChar] = 'a';
    }

    long dataSize;
    dataSize = 0;

    struct UdpPacket data;
    data = set_tdma_packet(pck_no, sdbuf, mac_addr, 0, end);

    printf("%s\n", mac_addr);
    
    printf("Duration\tTotal Datagrams\tPacket Size\tTransfer\tBandwidth\n");

    while(1)
    {
        //gettimeofday(&start, NULL);
        /*
        * Receive a packet from sfd, that the data should be put into line
        * line can store at most BUFLEN characters
        * The zero parameter says that no special flags should be used
        * Data about the sender should be stored in saddr, which has room for len byte
        */
        if ((numbytes = recvfrom(sfd, line, sizeof(struct TDMAControlPacket), 0, (struct sockaddr *)&saddr, &len)) == -1)
            diep("recvfrom()");

        //printf("TDMA Control Packet Size %d\n", numbytes);
        gettimeofday(&start, NULL);

        pck = (struct TDMAControlPacket *)line;

        slot_time = pck->slot_duration;
        //printf("%d\n", slot_time);
        line[numbytes] = '\0';

        strcpy(mac_addr, interface_mactoa());

        gettimeofday(&start, NULL);

        if(pck->type == 0 && join == 0)
        {
            //struct UdpPacket jpck;
            struct TDMAControlPacket jpck;

            //jpck = set_tdma_packet(pck_no, "JOIN", mac_addr, 0, start);
            jpck = set_control_packet(0, mac_addr, 1000);

            caddr = manage_sockaddr_in_send(SERVER_IP, JPORT);
            len = sizeof(caddr);
            if(sendto(sfd, (const char *)&jpck, sizeof(jpck), 0, (struct sockaddr *)&caddr, len) == -1)
                diep("send");

            join = 1;
        }
        else if(pck->type == 1 && join == 1 && strcmp(pck->id, mac_addr) == 0)
        {
            caddr = manage_sockaddr_in_send(SERVER_IP, DTPORT);
            len = sizeof(caddr);

            proc_time = 0;
            pack_count = 0;

            long duration_calc = 0;
	    gettimeofday(&start, NULL);

	    gettimeofday(&start, NULL);

            /*Send Data to Server*/
            while(1)
            {
                // while(dataSize < TOTAL_DATA_SIZE * 1024 * 1024) {
                gettimeofday(&end, NULL);
                proc_time = ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec));
                duration_calc = proc_time;
                //printf("%ld, ", proc_time);
                if(proc_time >= slot_time)
                    break;

                data.pck_id = pck_no;                            
                data.send_time_sec = end.tv_sec;
                data.send_time_usec = end.tv_usec;

                //gettimeofday(&t1, NULL);

                if(sendto(sfd, (const char *)&data, sizeof(data), 0, (struct sockaddr *)&caddr, len) == -1)
                    diep("sendto FILE:");

                //gettimeofday(&t2, NULL);

                //long diff = ((t2.tv_sec * 1000000 + t2.tv_usec) - (t1.tv_sec * 1000000 + t1.tv_usec));
                //printf("%d, ", sizeof(data));

                //printf("%ld.%ld\n", data.send_time_sec, data.send_time_usec);

                //dataSize += sizeof(data); // instead sdbuf, the size of the whole packet matters

		pck_no++;
                pack_count++;

                gettimeofday(&end, NULL);
                proc_time = ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec));
                duration_calc = proc_time;
                if(proc_time >= slot_time)
                    break;

                //pck_no++;
                //pack_count++;

                //usleep(SLEEP_TIME);

                //duration_calc = proc_time;
            }       

            struct UdpPacket dataTotal;
            dataTotal = set_tdma_packet(pack_count, "TOTAL", mac_addr, 9, end);
            
            // sending data to reciever to indicate that transmission ended 
            if(sendto(sfd, (const char *)&dataTotal, sizeof(dataTotal), 0, (struct sockaddr *)&caddr, len) == -1)
                diep("sendto FILE:");

            //printf("Finished | %d | %d | %d | %ld | %ld.%ld\n", slot_time, pck_no - 1, pack_count, dataSize, end.tv_sec, end.tv_usec);

            double total_data = pack_count * sizeof(data);
            double data_transferred = total_data/(1024 * 1024);
            //proc_time = proc_time - (pack_count * SLEEP_TIME);
            double bandwidth = (data_transferred * 8 * 1000000) / proc_time;

            printf("%ld\t%d\t%d\t%d\t%f\t%f\t%ld.%ld\n", duration_calc, pack_count, pck_no, sizeof(data), data_transferred, bandwidth, data.send_time_sec, data.send_time_usec);

            caddr = manage_sockaddr_in_send(SERVER_IP, JPORT);
            len = sizeof(caddr);

            struct UdpPacket end_data;
            //struct TDMAControlPacket end_data;
            end_data = set_tdma_packet(0, "END_TX", mac_addr, 1, end);
            //end_data = set_control_packet(9, mac_addr, 1000);
            // sending notification to broadcaster
            if(sendto(sfd, (const char *)&end_data, sizeof(end_data), 0, (struct sockaddr *)&caddr, len) == -1)
                diep("sendto");

            //if(dataSize >= TOTAL_DATA_SIZE * 1024 * 1024) quit = 1;
        }
        else if(pck->type == 1 && join == 1 && strcmp(pck->id, mac_addr) != 0) ;
            //printf("Waiting for Beacon|||\n");
        else if(pck->type == 0 && join == 1) ;
            //printf("Contention Slot|||\n");
        else if(pck->type == 1 && join == 0) ;
            //printf("Wrong Entry|||\n");
        else ;
            //printf("Don't Know what is happening|||\n");

        if(quit == 1) break;
    }

    close(sfd);

    return 0;
}
